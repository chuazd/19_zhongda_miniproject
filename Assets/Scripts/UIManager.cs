﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

	int score = 0;
	public Text scoreText;
	public Text sceneText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void IncrementScore(){
		score++;
		scoreText.text = "Score: " + score;
		if (score >= 25) {
			SceneManager.LoadScene ("Menu");
		}

		if (sceneText.text == "Level 1") {
			if (score >= 15) {
				SceneManager.LoadScene ("Level2");

			}
		}
	}
}
